# swayrandr

swayrandr and lswayrandr are programs to configure your screen-layouts in the Sway tiling window manager on Wayland. They are drop in replacements of xrandr and lxrandr respectively. They are intended to be used with the Sway tiling window manager on Wayland. They will not work under any other desktop environment (Pull Requests welcome!) nor will they work on X11.

This repository contains two programs: swayrandr and lswayrandr.

## Set-up

For both programs to work, you will need your screen-configuration in a separate file, like so:

```
# in ~/.config/sway/config:
include monitor.swayconfig
```

The default file to be modified is `~/.config/sway/monitor.swayconfig`. swayrandr will overwrite, whatever you hand to it!

## lswayrandr

Uses swayrandr to replace lxrandr. It allows choosing the screen configuration via a GTK3-based GUI.

![lswayrandr](resources/lswayrandr.png)

lswayrandr allows to set the resolution, placement and scale of each monitor. Fractional scaling is a feature of sway, that some applications implement well and others don't.

![Positioning of monitors](resources/positioning.png)

To set the positioning of a monitor it's best to think about it as a cross of monitors. You can set the left, middle and right monitor on the x-axis, and the above, middle and below monitor on the y-axis. This is where lswayrandr differs from lxrandr, as lswayrandr does not allow to set relations between monitors, but rather place them on a grid. This also means, working with more than 3 monitors on any axis is not supported.

## swayrandr

Replaces xrandr and works in a similar way. Without any parameters its output is similar -- listing the monitors and their available modes.

Parameters:
```
-version
    Print version Information
-help
    Print this help text
-config-file string
    Configuration file to write to (default "~/.config/sway/monitor.swayconfig")
-dryrun
    Don't write any configuration files, just dry-run and throw any occurring errors
-persist/-nopersist
    Persist the configuration into a cofnig-file

-listactivemonitors
    List all active monitors
-listmonitors
    List all connected monitors

-output string
    The output to manipulate
    -same-as string
        Place <output> on top of the provided monitor and use the same resolution if available
    -below string
        Place <output> below the provided monitor
    -above string
        Place <output> above the provided monitor
    -left-of string
        Place <output> left of the provided monitor
    -right-of string
        Place <output> right of the provided monitor
    -disable/-inactive/-off
        Disable <output>
    -enable
        Enable <output>
    -mode string
        Set the provided mode: <Width>x<Height>@<RefreshRate>
    -position string
        Set the top-left corner of <ouput> to the coordinates <X>x<Y> (or <X>+<Y>)
    -primary
        Set <output> as the primary one (focused)
    -refresh float
        Set the refresh rate (in KHz; float) (default -Inf)
    -scale float
        Set the scaling-factor of <output>; floats > 0 (default -Inf)
```

## Installation

### Arch Linux

Install from [AUR](https://aur.archlinux.org/packages/swayrandr).

### Other Distributions

* Everyone else (`<repo>` contains the checked out Repository):
```bash
#!/bin/bash
git clone https://codeberg.org/snaums/swayrandr.git

# builds both the CLI and GUI app
make dist

# Install the two applications onto your system (or don't)
sudo make install
```


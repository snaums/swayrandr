package main

import (
    "fmt"
    //"flag"
    . "codeberg.org/snaums/swayrandr/lib"
);

func main () {
    i, err := ReadInputs()
    if err != nil {
        fmt.Println ("Nah!");
        return
    }

    for _, v := range i {
        fmt.Println ( v.Identifier, "\t", v.Vendor, v.Product, v.Type );
    }
}


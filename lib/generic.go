package lib

import (
    "io"
    "fmt"
    "bytes"
    "os/exec"
    "strings"
    "encoding/json"
)

// mock-up of a string-builder
type mockerBuilder struct {
    strings.Builder
}

func (m* mockerBuilder) Write(p []byte) (n int, err error) {
    m.append ( string(p) )
    return 0,nil
}

func (m* mockerBuilder) append ( args... string) {
    for _, str := range args {
        m.WriteString ( str );
    }
}

// helper function for demarshalling JSON generically
func GenericJSONDecode ( rd io.Reader, pl interface{} ) error {
	dec := json.NewDecoder( rd )
	for dec.More() {
		err := dec.Decode( pl )
		if err != nil {
            return err
		}
	}
	return nil
}

// structure wrapping `swaymsg -t get_version -r`; only "HumanReadable" is used atm
type SwayVer struct {
    HumanReadable string `json:"human_readable"`    // human readable Version string (like v1.5)
    Variant string      // dunno
    Major int           // dunno, returns 0
    Minor int           // dunno, returns 0
    Patch int           // dunno, returns 0
    LoadedConfigFileName string `json:"loaded_config_file_name"`    // the loaded config file (main config; not listing its include-files)
}

// read the version Information from swaymsg and return the human-readable version string
func SwayVersion () string {
    var ret SwayVer
    err := QuerySway ( "get_version", &ret )
    if err != nil {
        return err.Error();
    }
    return ret.HumanReadable;
}

func QuerySway ( method string, pl interface{} ) error {
    out, _ := exec.Command ("swaymsg", "-t", method, "-r").CombinedOutput();
    return GenericJSONDecode ( bytes.NewReader ( out ), pl );
}

// tell sway to reload the configuration (i.e. reconfigure the monitor set-up, after the config-files where updated)
func ReloadSway () {
    exec.Command ("swaymsg", "reload" ).CombinedOutput();
}

func MsgSway( configFile string ) {
    lines := strings.Split( configFile, "\n" );
    for _, l := range lines {
        fmt.Println(l);
        c:=strings.Split(l, " ");
        exec.Command("swaymsg", c... ).CombinedOutput();
    }
}

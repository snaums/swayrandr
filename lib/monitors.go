// library package wrapping swaymsg concerning monitor layout and some helper functions
package lib

import (
    "fmt"
    "math"
    "errors"
    "strings"
    "strconv"
)

// structure for the modes of a monitor
type ScreenMode struct {
    Width int       // width in pixel in that mode
    Height int      // height in pixel
    Refresh int     // refresh rate in that resolution in Hz
}

// structure describing the rectangle a monitor takes up
// in the whole virtual framebuffer
type ScreenRect struct {
    X int           // x position for the monitor
    Y int           // y position for the monitor
    Width int       // Width in pixel
    Height int      // Height in pixel
}

// structre describing a monitor; wraps the return value
// of `swaymsg -t get_outputs -r`;
// the JSON-structure is probably used for more than output-objects in swaymsg
type Screen struct {
    Id int                  // Id of that monitor
    Name string             // Name (connection) like HDMI-1
    Rect ScreenRect         // Describes the rectangle of the monitor
    WindowRect ScreenRect   // dunno
    DecoRect ScreenRect     // dunno
    Geometry ScreenRect     // dunno
    Border string           // dunno
    CurrentBorderWidth int  // dunno
    Layout string           // dunno
    Orientation string      // horizontally or vertically
    Percent float64         // dunno
    Window string           // dunno -- mostly empty?
    Urgent bool             // dunno
    Marks []struct{}        // dunno
    FullscreenMode bool     // dunno
    Nodes []struct{}        // dunno
    FloatingNodes []struct{}// dunno
    Sticky bool             // dunno
    Type string             // dunno
    Active bool             // is the monitor enabled or disabled=
    Dpms bool               // dunno
    Primary bool            // is it the primary monitor (different from focused on boot-up)
    Make string             // the make of the monitor
    Model string            // the model of the monitor
    Serial string           // serial number of the monitor
    Scale float64           // float for scaling the monitor
    ScaleFilter string      // describes the filter used for scaling the monitor
    Transform string        // transformations on the output (rotation)
    AdaptiveSyncStatus string   // duno
    CurrentWorkspace string     // current workspace on that monitor
    Modes []ScreenMode          // lists the available modes on the monitor; same resolutions may have differing refresh rates; rates are given in Hz
    CurrentMode ScreenMode `json:"current_mode"`    // dunno
    MaxRenderTime int           // dunno
    Focused bool                // is the monitor focused on boot-up?
    SubpixelHinting string      // dunno
}

// structure describing Config Options for background per monitor
// not used atm
type ScreenBg struct {
    Path string
    Fillmode string
}

// one ConfigScreen is used per Monitor to describe its _new_ configuration to be set in the config-files
type ConfigScreen struct {
    Screen Screen           // "Reference" to the screen-object
    Mode ScreenMode         // the mode to be selected 
    Rect ScreenRect         // (only x,y used): describe the position of the screen
    Background ScreenBg     // background image (not used atm)
    Scale float64           // scale the contents of the monitor
    Enabled bool            // is the monitor enabled (i.e. on)?
    Focused bool            // will it be focused on boot-up of sway?
}

// Configurations are used for setting the
// outputs in the config-files for sway
type Config struct {
    cfg []ConfigScreen
}

// read the screen information from swaymsg and return an Array of Screen-Structures
func ReadScreen () ([]Screen, error) {
    var x []Screen
    err := QuerySway ( "get_outputs", &x )
    return x, err;
}

// Print the available modes of all screens, similar to xrandr's default output
func PrintModes ( screens []Screen ) {
    for _,s := range screens {
        fmt.Printf ( "%s connected %dx%d+%d+%d (normal left inverted right x axis y axis ) 0mm x 0mm\n", s.Name, s.Rect.Width, s.Rect.Height, s.Rect.X, s.Rect.Y );
        for _, m := range s.Modes {
            sel := " ";
            if m.Width == s.CurrentMode.Width && 
               m.Height == s.CurrentMode.Height &&
               m.Refresh == s.CurrentMode.Refresh {
                sel = "*";
            }
            fmt.Printf (" %s %5dx%5d\t%.2f+\n", sel, m.Width, m.Height, (float64(m.Refresh)/1000.0) );
        }
    }
}

// create config from updated screen-information
func CreateConfig ( screens []Screen ) Config {
    var cfg Config;
    for _, s := range screens {
        var c ConfigScreen
        c.Screen = s;
        c.Mode = s.CurrentMode;
        c.Rect = s.Rect;
        c.Scale = math.Round(s.Scale*10)/10;
        c.Enabled = s.Active;
        c.Focused = s.Focused;

        cfg.cfg = append ( cfg.cfg, c );
    }

    return cfg
}

// find screen-structure by the connection name (Screen.Name)
func FindScreen ( screens []Screen, output string ) *Screen {
    if output == "" {
        return nil;
    }

    for i := 0; i < len(screens); i++ {
        s := &screens[i];
        if s.Name == output {
            return s;
        }
    }
    return nil;
}

// Build the config-command-string (output and focus-commands)
// for writing into the monitor-config-file
func PrintConfig ( cfg Config ) string {
    var mc mockerBuilder
    for _, c := range cfg.cfg {
        if c.Enabled == true {
            mc.append ("output ", c.Screen.Name,
                " mode ", strconv.Itoa(c.Mode.Width),
                    "x", strconv.Itoa(c.Mode.Height), 
                    "@", strconv.Itoa(c.Mode.Refresh), "Hz",
                " pos ", strconv.Itoa(c.Rect.X), " ", strconv.Itoa(c.Rect.Y),
                " scale ", strconv.FormatFloat(c.Scale, 'f', -1, 64),
                "\n");
            if c.Focused == true {
                mc.append ("focus output ", c.Screen.Name, "\n" );
            }
        } else {
            mc.append ( "output ", c.Screen.Name, " disable\n");
        }
    }

    var buf string = mc.String()
    return buf;
}

// process positioning parameters to the SwayRandr program (left-of, right-of, above and below);
// sets the x and y options of the involved screens accordingly
func ProcessPosition ( outputScreen *Screen, leftOf string, rightOf string, above string, below string, screens []Screen ) error {
    left := FindScreen ( screens, leftOf );
    right := FindScreen ( screens, rightOf );

    if above == "" && below == "" {
        outputScreen.Rect.Y = 0;
        if left != nil {
            left.Rect.Y = 0;
        }
        if right != nil {
            right.Rect.Y = 0;
        }
    }

    if left == outputScreen || right == outputScreen {
        return errors.New("Cannot fulfil constraint; output left or right of output");
    }

    if leftOf != "" && rightOf != "" {
        if left == nil || right == nil {
            return errors.New ("Cannot find outputs " + leftOf + " or " + rightOf );
        }
        if left == right {
            return errors.New("Cannot fulfil constraint; left == right");
        }

        if len(screens) == 3 {
            right.Rect.X = 0;
        }
    }

    if rightOf != "" {
        if right == nil {
            return errors.New ("Cannot find output "+ rightOf );
        }

        if len(screens) == 2 {
            right.Rect.X = 0
        }
        outputScreen.Rect.X = right.Rect.X + right.Rect.Width
    }

    if leftOf != "" {
        if left == nil {
            return errors.New ("Cannot find output "+ leftOf );
        }

        if len(screens) == 2 {
            outputScreen.Rect.X = 0
        }
        left.Rect.X = outputScreen.Rect.X + outputScreen.Rect.Width
    }

    a := FindScreen ( screens, above );
    b := FindScreen ( screens, below );

    if leftOf == "" && rightOf == "" {
        outputScreen.Rect.X = 0;
        if a != nil {
            a.Rect.X = 0;
        }

        if b != nil {
            b.Rect.X = 0;
        }
    }

    if a == outputScreen || b == outputScreen {
        return errors.New("Cannot fulfil constraint; output above or below of output");
    }

    if above != "" && below != "" {
        if a == nil || b == nil {
            return errors.New ("Cannot find outputs " + above + " or " + below );
        }
        if a == b {
            return errors.New("Cannot fulfil constraint; above == below");
        }

        if len(screens) == 3 {
            a.Rect.Y = 0;
        }
    }

    if below != "" {
        if b == nil {
            return errors.New ("Cannot find output "+ below );
        }

        if len(screens) == 2 {
            b.Rect.Y = 0
        }
        outputScreen.Rect.Y = b.Rect.Y + b.Rect.Height
    }

    if above != "" {
        if a == nil {
            return errors.New ("Cannot find output "+ above );
        }

        if len(screens) == 2 {
            outputScreen.Rect.X = 0
        }
        a.Rect.Y = outputScreen.Rect.Y + outputScreen.Rect.Height
    }
    return nil;
}

// sets the mode for a Screen;
// takes the refresh rate both from the mode and the refresh parameter to SwayRandr
// the parameter takes precedence
func ProcessMode ( outputScreen *Screen, mode string, refresh float64 ) error {
    var r int
    refreshDC := false;
    if math.IsInf ( refresh, 0 ) == true {
        refreshDC = true;
    } else {
        r = int( refresh*1000 );
    }

    if mode == "" && refreshDC == true {
        return nil;
    }

    var w, h int
    x := strings.Split ( mode, "x" );
    w, err := strconv.Atoi ( x[0] );
    if err != nil {
        return err;
    }

    if strings.Index ( x[1], "@" ) == -1 {
        h, err = strconv.Atoi ( x[1] );
        if err != nil {
            return err;
        }
    } else {
        if refreshDC == false {
            return errors.New ("Mode specified a refresh rate and one is explicitely given");
        }

        xy := strings.Split ( x[1], "@" );
        h, err = strconv.Atoi ( xy[0] );
        if err != nil {
            return err;
        }

        refresh, err = strconv.ParseFloat ( xy[1], 64 );
        r = int ( refresh*1000 );
        refreshDC = false;
    }

    for _, m := range outputScreen.Modes {
        if refreshDC == true {
            if w == m.Width && h == m.Height {
                outputScreen.Rect.Width = w;
                outputScreen.Rect.Height = h;
                outputScreen.CurrentMode = m;
                return nil;
            }
        } else {
            if w == m.Width && h == m.Height && r == m.Refresh  {
                outputScreen.Rect.Width = w;
                outputScreen.Rect.Height = h;
                outputScreen.CurrentMode = m;
                return nil;
            }
        }
    }

    if refreshDC == true {
        return errors.New ("Cannot find the mode specified: "+strconv.Itoa(w)+"x"+strconv.Itoa(h));
    } else {
        return errors.New ("Cannot find the mode and refresh rate specified: " + strconv.Itoa ( w) + "x" + strconv.Itoa(h) + "@"+strconv.Itoa(r)+"Hz");
    }
}

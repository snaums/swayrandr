package lib

type LibInput struct {
    SendEvent string `json:'send_event'`
    AccelSpeed float64
    AccelProfile string
    NaturalScroll string
    LeftHanded string
    MiddleEmulation string
    ScrollMethod string
    ScrollButton int
    Tap string
    TapButtonMap string
    TapDrag string
    TapDragLock string
    ClickMethod string
    Dwt string
}

type Inputs struct {
    Identifier string
    Name string
    Vendor int
    Product int
    Type string
    XkbLayoutNames []string `json:'xkb_layout_names'`
    XkbActiveLayoutIndex int
    XkbActiveLayoutName string
    Libinput LibInput
}

func ReadInputs () ([]Inputs, error) {
    var x []Inputs
    err := QuerySway ( "get_inputs", &x );
    return x, err;
}

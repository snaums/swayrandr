VERSION=v$(shell git describe --tags --always)
BUILDVARS=-X 'main.Version=$(VERSION)' -X 'main.BuildDate=$(shell date)' -X 'main.BuildWith=$(shell go version)'
GLADEVAR=-X 'main.GladeString=$(shell sed 's/\"/\\\"/g' ui.glade)'
DESTDIR ?=

LIB:= $(shell find lib -name '*.go')

all: swayrandr

input: swayinput
dist: swayrandr lswayrandr swayinput

ui: lswayrandr

install: swayrandr lswayrandr
	install -Dm755 swayrandr $(DESTDIR)/usr/bin/swayrandr
	install -Dm755 lswayrandr $(DESTDIR)/usr/bin/lswayrandr

uninstall:
	rm $(DESTDIR)/usr/bin/swayrandr
	rm $(DESTDOR)/usr/bin/lswayrandr

swayinput: swayinput.go $(LIB)
	go build -o swayinput -ldflags "-s -w $(BUILDVARS)" swayinput.go

lswayrandr: ui.go lib/monitors.go ui.glade
	@echo -e "\t GO build \t ui.go"
	@go build -o lswayrandr -ldflags "-s -w $(BUILDVARS) $(GLADEVAR)" ui.go

swayrandr: main.go lib/monitors.go
	@echo -e "\t GO build \t main.go"
	@go build -o swayrandr -ldflags "-s -w $(BUILDVARS)" main.go

clean:
	-rm swayrandr lswayrandr swayinput

dep:
	go get -v github.com/gotk3/gotk3/gtk

run:
	go run *.go
